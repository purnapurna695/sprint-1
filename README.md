
Scrum è un framework di lavoro che fa strettamente affidamento su ogni componente per raggiungere un’obiettivo.
Ha come caratteristiche :
- un processo di controllo empirico
- incoraggiamento all’auto-organizzazione
- collaborazione
- prioritizzazione in base al valore
- inscatolamento-temporale
- sviluppo iterativo
